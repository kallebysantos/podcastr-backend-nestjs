import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Delete,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Admin, Public } from 'src/auth/constants';
import { JWTRequest } from 'src/auth/jwt.strategy';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    const created = await this.usersService.create(createUserDto);

    return this.usersService.parseToRead(created);
  }

  @Get()
  async findOne(@Request() { user }: JWTRequest) {
    const found = await this.usersService.findOne(user.id);

    return this.usersService.parseToRead(found);
  }

  @Patch()
  async update(
    @Request() { user }: JWTRequest,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    const updated = await this.usersService.update(user.id, updateUserDto);

    return this.usersService.parseToRead(updated);
  }

  @Admin()
  @Delete()
  remove(@Request() { user }: JWTRequest) {
    return this.usersService.remove(user.id);
  }
}
