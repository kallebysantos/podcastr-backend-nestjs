import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column('boolean')
  isAdmin: boolean;

  @Column({ nullable: true })
  image?: string;

  @Column({ nullable: true })
  folderId?: string;
}
