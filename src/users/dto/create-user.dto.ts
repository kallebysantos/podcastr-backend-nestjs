import { IsString, IsEmail, Length } from 'class-validator';

export class CreateUserDto {
  @Length(3)
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @Length(5)
  @IsString()
  password: string;
}
