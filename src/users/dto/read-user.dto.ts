export class ReadUserDto {
  id: number;

  name: string;

  email: string;

  isAdmin: boolean;

  image?: string;
}
