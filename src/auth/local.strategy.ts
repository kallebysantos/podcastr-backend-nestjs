import {
  BadRequestException,
  forwardRef,
  Inject,
  UnauthorizedException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { validate } from 'class-validator';
import { Strategy } from 'passport-local';
import { AuthLoginDto } from './auth-login.dto';
import { AuthService } from './auth.service';

export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
  ) {
    super();
  }

  async validate(email: string, password: string): Promise<any> {
    const authValues = new AuthLoginDto();
    authValues.username = email;
    authValues.password = password;

    const errors = await validate(authValues);
    if (errors.length > 0) {
      const message = errors.map((error) => ({
        property: error.property,
        description: Object.values(error.constraints)[0],
      }));

      throw new BadRequestException(message);
    }

    const user = await this.authService.validateUser({ email, password });

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
