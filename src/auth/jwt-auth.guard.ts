import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { verify } from 'jsonwebtoken';
import { IS_ADMIN_KEY, IS_PUBLIC_KEY, jwtConstants } from './constants';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) {
      return true;
    }

    const isAdmin = this.reflector.getAllAndOverride<boolean>(IS_ADMIN_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isAdmin) {
      const bearerToken = context
        .switchToHttp()
        .getRequest()
        .headers.authorization.split(' ')[1];

      const decoded = verify(bearerToken, jwtConstants.secret) as any;

      if (!decoded.admin) return false;
    }

    return super.canActivate(context);
  }
}
