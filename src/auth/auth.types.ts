import { ReadUserDto } from 'src/users/dto/read-user.dto';

export interface ValidadeUserProps {
  email: string;
  password: string;
}

export interface IAuthService {
  validateUser(values: ValidadeUserProps): Promise<ReadUserDto | null>;
}
