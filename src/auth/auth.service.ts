import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcryptjs';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { IAuthService, ValidadeUserProps } from './auth.types';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser({ email, password }: ValidadeUserProps) {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) return null;

    const isPasswordMatch = await compare(password, user.password);
    if (!isPasswordMatch) return null;

    return this.usersService.parseToRead(user);
  }

  async login(user: User) {
    const payload = {
      email: user.email,
      sub: user.id,
      admin: user.isAdmin,
    };

    return {
      token: this.jwtService.sign(payload),
      user,
    };
  }
}
