import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { parseBuffer } from 'music-metadata';
import { fromBuffer } from 'file-type';
import { CreatePodcastDto } from './dto/create-podcast.dto';
import { UpdatePodcastDto } from './dto/update-podcast.dto';
import { Podcast } from './entities/podcast.entity';
import { IStorageService } from 'src/helpers/storage/storage.types';
import { StorageService } from 'src/helpers/storage/storage.service';
@Injectable()
export class PodcastsService {
  private readonly storageService: IStorageService;

  constructor(
    @InjectRepository(Podcast)
    private podcastRepository: Repository<Podcast>,
  ) {
    this.storageService = new StorageService();
  }

  async create({ name, members, description, audio, image }: CreatePodcastDto) {
    const folderId = uuid();
    const folder = await this.storageService.useFolder(`/${folderId}`);

    const { duration } = (await parseBuffer(audio.buffer)).format;
    const { ext: audioExt } = await fromBuffer(audio.buffer);
    const storedAudio = await this.storageService.storeFile({
      file: audio,
      destination: `${folder.location}/audio.${audioExt}`,
    });

    const { ext: imageExt } = await fromBuffer(image.buffer);
    const storedImage = await this.storageService.storeFile({
      file: image,
      destination: `${folder.location}/thumbnail.${imageExt}`,
    });

    const podcast = this.podcastRepository.create({
      name,
      members,
      description,
      folderId,
      duration,
      audio: storedAudio.sharedLink,
      image: storedImage.sharedLink,
      publishedAt: new Date().toDateString(),
    });

    return this.podcastRepository.save(podcast);
  }

  findAll() {
    return this.podcastRepository.find();
  }

  async findOne(id: number) {
    const result = await this.podcastRepository.findOne(id);

    if (!result) {
      throw new HttpException('Podcast not found', HttpStatus.NOT_FOUND);
    }

    return result;
  }

  async update(id: number, updatePodcastDto: UpdatePodcastDto) {
    const podcast = await this.podcastRepository.findOne(id);

    if (!podcast) {
      throw new HttpException('Podcast not found', HttpStatus.NOT_FOUND);
    }

    Object.assign(podcast, updatePodcastDto);

    return this.podcastRepository.save(podcast);
  }

  async remove(id: number) {
    const podcast = await this.podcastRepository.findOne(id);

    if (!podcast) {
      throw new HttpException('Podcast not found', HttpStatus.NOT_FOUND);
    }

    this.storageService.deleteFolder(`/${podcast.folderId}`);

    return this.podcastRepository.delete(id);
  }
}
