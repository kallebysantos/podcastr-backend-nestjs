import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { PodcastsService } from './podcasts.service';
import { UpdatePodcastDto } from './dto/update-podcast.dto';
import { UploadPodcastDto } from './dto/upload-podcast.dto';
import { PostPodcastDto } from './dto/post-podcast.dto';
import { Admin } from 'src/auth/constants';

@Controller('podcasts')
export class PodcastsController {
  constructor(private readonly podcastsService: PodcastsService) {}

  @Admin()
  @Post()
  @UseInterceptors(
    FileFieldsInterceptor([
      {
        name: 'audio',
        maxCount: 1,
      },
      {
        name: 'image',
        maxCount: 1,
      },
    ]),
  )
  async create(
    @UploadedFiles() files: UploadPodcastDto,
    @Body() { name, members, description }: PostPodcastDto,
  ) {
    const audio = files.audio[0];
    const image = files.image[0];

    if (
      audio.mimetype !== 'audio/mpeg' &&
      audio.mimetype !== 'audio/ogg' &&
      audio.mimetype !== 'audio/wavpack'
    ) {
      throw new HttpException('Invalid audio type', HttpStatus.BAD_REQUEST);
    }

    if (image.mimetype !== 'image/jpeg' && image.mimetype !== 'image/png') {
      throw new HttpException('Invalid image type', HttpStatus.BAD_REQUEST);
    }

    return this.podcastsService.create({
      name,
      members,
      description,
      audio,
      image,
    });
  }

  @Get()
  findAll() {
    return this.podcastsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.podcastsService.findOne(+id);
  }

  @Admin()
  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePodcastDto: UpdatePodcastDto) {
    return this.podcastsService.update(+id, updatePodcastDto);
  }

  @Admin()
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.podcastsService.remove(+id);
  }
}
