import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('podcasts')
export class Podcast {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column('float')
  duration: number;

  @Column('text')
  description: string;

  @Column()
  members: string;

  @Column()
  audio: string;

  @Column()
  image: string;

  @Column()
  folderId: string;

  @Column()
  publishedAt: string;
}
