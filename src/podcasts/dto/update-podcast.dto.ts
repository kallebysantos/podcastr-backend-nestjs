import { PartialType } from '@nestjs/mapped-types';
import { PostPodcastDto } from './post-podcast.dto';

export class UpdatePodcastDto extends PartialType(PostPodcastDto) {}
