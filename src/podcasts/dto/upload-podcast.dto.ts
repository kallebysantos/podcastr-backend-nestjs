export class UploadPodcastDto {
  audio: [Express.Multer.File];

  image: [Express.Multer.File];
}
