export class CreatePodcastDto {
  name: string;

  description: string;

  members: string;

  audio: Express.Multer.File;

  image: Express.Multer.File;
}
