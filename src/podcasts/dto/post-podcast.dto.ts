import { IsString, Length } from 'class-validator';

export class PostPodcastDto {
  @Length(10)
  @IsString()
  name: string;

  @Length(10)
  @IsString()
  description: string;

  @Length(10)
  @IsString()
  members: string;
}
