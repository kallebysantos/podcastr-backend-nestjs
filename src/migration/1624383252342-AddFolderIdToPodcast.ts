import {MigrationInterface, QueryRunner} from "typeorm";

export class AddFolderIdToPodcast1624383252342 implements MigrationInterface {
    name = 'AddFolderIdToPodcast1624383252342'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "podcasts" ADD "folderId" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "podcasts" DROP COLUMN "folderId"`);
    }

}
