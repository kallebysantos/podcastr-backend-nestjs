import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPodcast1624375006766 implements MigrationInterface {
  name = 'AddPodcast1624375006766';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "podcasts" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "duration" integer NOT NULL, "description" text NOT NULL, "members" character varying NOT NULL, "audio" character varying NOT NULL, "image" character varying NOT NULL, "publishedAt" character varying NOT NULL, CONSTRAINT "PK_6df41936ccc877b29da54f11912" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "podcasts"`);
  }
}
