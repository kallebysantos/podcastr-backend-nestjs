import {MigrationInterface, QueryRunner} from "typeorm";

export class AddIsAdminFieldToUser1624403193721 implements MigrationInterface {
    name = 'AddIsAdminFieldToUser1624403193721'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "isAdmin" boolean NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "isAdmin"`);
    }

}
