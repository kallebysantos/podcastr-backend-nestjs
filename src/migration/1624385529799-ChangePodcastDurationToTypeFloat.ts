import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangePodcastDurationToTypeFloat1624385529799 implements MigrationInterface {
    name = 'ChangePodcastDurationToTypeFloat1624385529799'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "podcasts" DROP COLUMN "duration"`);
        await queryRunner.query(`ALTER TABLE "podcasts" ADD "duration" double precision NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "podcasts" DROP COLUMN "duration"`);
        await queryRunner.query(`ALTER TABLE "podcasts" ADD "duration" integer NOT NULL`);
    }

}
