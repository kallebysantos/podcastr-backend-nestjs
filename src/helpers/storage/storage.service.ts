import { Injectable } from '@nestjs/common';
import { Dropbox } from 'dropbox';
import {
  IStorageService,
  IStoredFile,
  IStoredFolder,
  StoreFileProps,
} from './storage.types';

@Injectable()
export class StorageService implements IStorageService {
  private cloudService: Dropbox;
  constructor() {
    this.cloudService = new Dropbox({
      accessToken: process.env.CLOUD_SECRET,
    });
  }

  async deleteFolder(path: string): Promise<boolean> {
    const { result } = await this.cloudService.filesDeleteV2({
      path,
    });

    return !!result;
  }

  async useFolder(path: string): Promise<IStoredFolder> {
    const { result } = await this.cloudService.filesCreateFolderV2({ path });

    return {
      id: result.metadata.id,
      name: result.metadata.name,
      location: result.metadata.path_lower,
    };
  }

  async storeFile({ file, destination }: StoreFileProps): Promise<IStoredFile> {
    const { result: uploaded } = await this.cloudService.filesUpload({
      contents: file.buffer,
      path: destination,
      autorename: true,
      strict_conflict: true,
    });

    const { result: shared } =
      await this.cloudService.sharingCreateSharedLinkWithSettings({
        path: uploaded.path_lower,
      });

    return {
      id: shared.id,
      name: shared.name,
      size: uploaded.size,
      location: shared.path_lower,
      sharedLink: shared.url.replace('?dl=0', ''),
    };
  }
}
