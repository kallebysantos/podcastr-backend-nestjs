export interface IStoredFolder {
  id: string;
  name: string;
  location: string;
}

export interface IStoredFile {
  id: string;
  name: string;
  size: number;
  location: string;
  sharedLink?: string;
}

export interface StoreFileProps {
  file: Express.Multer.File;
  destination: string;
}

export interface IStorageService {
  deleteFolder(path: string): Promise<boolean>;
  useFolder(path: string): Promise<IStoredFolder>;
  storeFile(values: StoreFileProps): Promise<IStoredFile>;
}
